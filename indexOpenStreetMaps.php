<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mapas OPEN STREET MAPS</title>


    <!-- Leaflet's CSS -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
          integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
          crossorigin=""/>
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
            integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
            crossorigin=""></script>

    <script src='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet' />

</head>
<body>
<?php

include(".env.php");

//Url a consumir //// ENDPOINT
$url = "https://weatherapi-com.p.rapidapi.com/forecast.json?q=Guadalajara&days=3&lang=English";
//CURL

//inicializa el obj curl
$curl = curl_init();

//config el curl-------------------------------------

//curl URL a visitar/consumir
curl_setopt_array($curl, [
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => [
        "x-rapidapi-host: weatherapi-com.p.rapidapi.com",
        "x-rapidapi-key: ".RAPIDAPI_KEY
    ],
]);

//var_dump("iniciando...");
//Response del consumo
$response = curl_exec($curl);
$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

if(!$response)
{
    var_dump("ERROR, NO HAY RESPUESTA.--.-");
    return FALSE;
}


if($httpCode < 400)
{
    echo "Datos correctos <hr>";
    //var_dump( "response <hr> ");
    $resultado = json_decode($response, true);
    //var_dump($resultado['location']);
    $indexes = array_keys($resultado);
    //print_r($indexes);
    $lenght = count($indexes);


    echo "<hr>";
    $headersLocation = array_keys($resultado["location"]);

    $headers="";
    $data="";
    $counter= count($headersLocation);
    for($i=0; $i<$counter; $i++)
    {
        $headers .= "<th>".ucwords($headersLocation[$i])."</th>  ";
    }

    $lat='';
    $lon='';
    foreach ($resultado["location"] as $index=>$dato)
    {
        if($index=='lat') $lat=$dato;
        if($index=='lon') $lon=$dato;
        $data .= "<td>".$dato."</td>";
    }

    echo '
       
        <table border="1" align="center">
            <thead>
                <tr>
                    '.$headers. '
                </tr>
                <tr>                  
                    <div id="mapid" style="width: 100%; height: 400px;"></div>
                    <script>             
                        var mymap = L.map(\'mapid\').setView(['.$lat.', '.$lon.'], 13);                    
                        L.tileLayer(\'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token='.MAPBOX_API_KEY.'\', 
                        {
                            maxZoom: 18,
                            attribution: \'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, \' +
                                \'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>\',
                            id: \'mapbox/streets-v11\',
                            tileSize: 512,
                            zoomOffset: -1
                        }).addTo(mymap);                    
                       L.marker(['.$lat.', '.$lon.']).addTo(mymap)
                            .bindPopup("<b>Hola CLICK</b><br />Info en globo popup.").openPopup();                    
                       /* L.circle([51.508, -0.11], 500, {
                            color: \'red\',
                            fillColor: \'#f03\',
                            fillOpacity: 0.5
                        }).addTo(mymap).bindPopup("I am a circle.");                    
                        L.polygon([
                            [51.509, -0.08],
                            [51.503, -0.06],
                            [51.51, -0.047]
                        ]).addTo(mymap).bindPopup("I am a polygon.");     */                                     
                        var popup = L.popup();                  
                        function onMapClick(e) {
                            popup
                                .setLatLng(e.latlng)
                                .setContent("Hiciste click en... " + e.latlng.toString())
                                .openOn(mymap);
                        }
                        mymap.on(\'click\', onMapClick);                    
                    </script>
                </tr>
            </thead>
            <tbody>
                <tr>
                    ' .$data.'
                </tr>
            </tbody>
    ';
    echo "<hr>";
    echo "<hr>";

    //current forecast
    $current = $resultado["current"];
    $currentIdxs = array_keys($current);
    $headers="";
    $data="";
    foreach ($currentIdxs as $index=>$key)
    {
        if($index==5)
        {
            foreach (array_keys($current["condition"]) as $idx=>$headerCurrent)
            {
                $headers.="<th>$headerCurrent</th>";
            }
        }else
        $headers.="<th>$key</th>";
    }
    foreach ($current as $index=>$datoClima)
    {
        if($index=="condition")
        {
            //explotar condition
            foreach ($datoClima as $idx=>$datoCurrent)
            {
                if($idx=="icon")
                {
                    $data.="<th><img src='".str_replace("\\", "", $datoCurrent)."'></th>";
                }else
                    $data.="<th>$datoCurrent</th>";
               // var_dump($datoCurrent);
            }
        }else
            $data.="<th>$datoClima</th>";
    }

    $tabla = '
        <table border="1" align="center">
            <thead>
                <tr>'.$headers.'</tr>
            </thead>
            <tbody>
                <tr>'.$data.'</tr>            
            </tbody>
        </table>
    ';
    echo $tabla;
    echo "<hr>";
    echo "http code ".$httpCode;
    return TRUE;
}
else
{
    echo "ERROR response ".$response;
    echo "<hr>";
    echo "ERROR http code ".$httpCode;
    return FALSE;
}

?>

</body>
</html>