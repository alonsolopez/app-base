<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consumiendo APIs</title>



</head>
<body>
<?php

include(".env.php");
//parseo de ciudad
$ciudad = isset($_GET['ciudad'])?$_GET['ciudad']:'Phoenix';
//Url a consumir //// ENDPOINT
$url = "https://weatherapi-com.p.rapidapi.com/forecast.json?q=".$ciudad."&days=3&lang=English";
//CURL

//inicializa el obj curl
$curl = curl_init();

//config el curl-------------------------------------

//curl URL a visitar/consumir
curl_setopt_array($curl, [
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => [
        "x-rapidapi-host: weatherapi-com.p.rapidapi.com",
        "x-rapidapi-key: ".RAPIDAPI_KEY
    ],
]);

//var_dump("iniciando...");
//Response del consumo
$response = curl_exec($curl);
$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

if(!$response)
{
    var_dump("ERROR, NO HAY RESPUESTA.--.-");
    return FALSE;
}


if($httpCode < 400)
{
    echo "Datos correctos <hr>";
    //var_dump( "response <hr> ");
    $resultado = json_decode($response, true);
    //var_dump($resultado['location']);
    $indexes = array_keys($resultado);
    //print_r($indexes);
    $lenght = count($indexes);


    echo "<hr>";
    $headersLocation = array_keys($resultado["location"]);

    $headers="";
    $data="";
    $counter= count($headersLocation);
    for($i=0; $i<$counter; $i++)
    {
        $headers .= "<th>".ucwords($headersLocation[$i])."</th>  ";
    }

    foreach ($resultado["location"] as $index=>$dato)
    {
        $data .= "<td>".$dato."</td>";
    }

    echo '
        <h1>LOCATION</h1>
        <table border="1" align="center">
            <thead>
                <tr>
                    '.$headers. '
                </tr>
               
            </thead>
            <tbody>
                <tr>
                    ' .$data.'
                </tr>
            </tbody>
    ';

    echo "<hr>";

    //current forecast
    $current = $resultado["current"];
    $currentIdxs = array_keys($current);
    $headers="";
    $data="";
    foreach ($currentIdxs as $index=>$key)
    {
        if($index==5)
        {
            foreach (array_keys($current["condition"]) as $idx=>$headerCurrent)
            {
                $headers.="<th>$headerCurrent</th>";
            }
        }else
        $headers.="<th>$key</th>";
    }
    foreach ($current as $index=>$datoClima)
    {
        if($index=="condition")
        {
            //explotar condition
            foreach ($datoClima as $idx=>$datoCurrent)
            {
                if($idx=="icon")
                {
                    $data.="<td><img src='".str_replace("\\", "", $datoCurrent)."'></td>";
                }
            }
        }else
            $data.="<td>$datoClima</td>";
    }

    $tabla = '
        <h1>CURRENT</h1>
        <table border="1" align="center">
            <thead>
                <tr>'.$headers.'</tr>
            </thead>
            <tbody>
                <tr>'.$data.'</tr>            
            </tbody>
        </table>
    ';
    echo $tabla;
    echo "<hr>";
    echo "http code ".$httpCode;
    return TRUE;
}
else
{
    echo "ERROR response ".$response;
    echo "<hr>";
    echo "ERROR http code ".$httpCode;
    return FALSE;
}

?>

</body>
</html>
